// $Id$

Nodequeue Scheduler module
-------------------------------------------------------------------------------

This module allows you to specify a publication schedule for nodequeues. 

INSTALLATION:
-------------------------------------------------------------------------------

Install as usual, see http://drupal.org/node/70151 for further information.


Maintainers
-------------------------------------------------------------------------------
Module is fully developped, sponsored and maintained by Adyax.

Current maintainer: Ivan Tsekhmistro - itsekhmistro@adyax.com
